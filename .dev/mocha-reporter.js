const mocha = require('mocha');


const { EVENT_TEST_FAIL } = mocha.Runner.constants;

// Simple reporter to log only failed tests.
module.exports = function(runner) {
	mocha.reporters.Base.call(this, runner);
	console.log();

	const title = test => {
		const segments = [];
		while(test) {
			segments.push(test.title);
			test = test.parent
		}
		return segments.filter(s => s.length).reverse().join(' | ');
	}

	runner.on(EVENT_TEST_FAIL, (test, err) => {
		const stack = err.stack.split('\n')[1];
		console.error(` ✖ ${title(test)}\n   ${err.message}\n ${stack}\n`);
	});
}