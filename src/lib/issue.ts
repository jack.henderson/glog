export type Issue = {
	reference: string;
	closes: boolean;
};