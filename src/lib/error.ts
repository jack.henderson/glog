const glogErrorTag = Symbol();

export class GlogError extends Error {
	public readonly status: number;
	private readonly _glogTag = glogErrorTag;

	constructor(message: string, status?: number | null) {
		super(message);
		this.status = status || 1;
	}

	public static is = (err: unknown): err is GlogError => (
		err
			&& typeof(err) === 'object'
			&& '_glogTag' in err
			&& (err as { _glogTag: unknown })._glogTag === glogErrorTag
	);
}