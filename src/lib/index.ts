export { Commit, RegularCommit, MergeCommit, RevertCommit } from './commit/';
export { 
	assertGitIsInstalled,
	gitBranches,
	gitCurrentBranch,
	gitVersion,
} from './git';
