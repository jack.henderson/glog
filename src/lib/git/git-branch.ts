import { git } from './git';

/** Get a list of all git branches. */
export const gitBranches = (): string[] => (
	git('branch')
		.split('\n')
		.map(s => s.replace('*', ''))
		.map(s => s.trim())
		.filter(s => s.length)
);

/** Get the name of the current git branch. */
export const gitCurrentBranch = (): string => (
	git('branch')
		.split('\n')
		.filter(s => s.startsWith('*'))
		.map(s => s.replace('* ', '').trim())[0]
);