import { RawCommit } from '../commit';
import { git } from './git';


export const gitLog = (...options: string[]): RawCommit[] => (
	git('log', `--pretty=${prettyFormat}`, ...options)
		.split(COMMIT_DELIMITER)
		.slice(0, -1)
		.map(parseCommit)
);

export const gitLogUnique = (sourceBranch: string, targetBranch: string): RawCommit[] => (
	gitLog(`${targetBranch}..${sourceBranch}`)
);


const COMMIT_DELIMITER = '__COMMIT__';

type TokenName = keyof RawCommit;
type TokenAndPlaceholder = [ token: TokenName, placeholder: string ];
const formatTokens: TokenAndPlaceholder[] = [
	[ 'hash', '%H' ],
	[ 'ref', '%h'],
	[ 'subject', '%s' ],
	[ 'body', '%b' ],
	[ 'date', '%aI' ],
];

const beginToken = (token: keyof RawCommit) => `__begin_${token}__`;
const endToken   = (token: keyof RawCommit) => `__end_${token}__`;

const prettyFormat = (
	formatTokens
		.reduce((accum, [ token, placeholder ]) => (
			accum + `${beginToken(token)}\n${placeholder}\n${endToken(token)}\n`
		), '') + `\n${COMMIT_DELIMITER}`
);

const parseCommit = (commit: string): RawCommit => {
	const lines = commit.split('\n');
	const result: Partial<RawCommit> = {};
	
	formatTokens.forEach(([ token ]) => {
		const begin = lines.indexOf(beginToken(token)) + 1;
		const end = lines.indexOf(endToken(token));

		const part = lines
			.slice(begin, end)
			.join('\n');

			switch(token) {
				case 'date':
					result.date = new Date(part);
					break;
				default:
					result[token] = part;
			}
	});
	
	return result as RawCommit;
};
