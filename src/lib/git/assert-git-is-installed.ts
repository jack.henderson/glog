import { spawnSync } from 'child_process';
import { GlogError } from '../error';

/**
 * Throw a `GlogError` if the `git` binary does not appear to be installed.
 */
export const assertGitIsInstalled = (): void => {
	const result = spawnSync('git');
	if(result.error) {
		if('code' in result.error && result.error['code'] === 'ENOENT') {
			throw new GlogError('Git does not appear to be installed.');
		}
		throw result.error;
	}
};