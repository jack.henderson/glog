import { spawnSync } from 'child_process';
import { GlogError } from '../error';

/**
 * Synchronously execute a `git` command in a new process.
 * 
 * For example:
 *  - `git('status');`
 *  - `git('--version');`
 *  - `git('commit', '-m' '"example"');`
 * 
 * @param command git command to execute.
 * @returns git's stdout.
 * @throws `GlogError` if git exits with non-zero exit code. The error's
 * 	`message` property will contain the content of stderr. The error's
 *  `status` property will be git's exit code.
 */
export const git = (...command: string[]): string => {
	const result = spawnSync('git', command, {
		windowsHide: true,
	});

	if(result.status !== 0) {
		throw new GlogError(result.stderr.toString(), result.status);
	}

	return result.stdout.toString();
};