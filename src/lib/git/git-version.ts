import { git } from './git';

/**
 * Return the version of git installed on the system.
 */
export const gitVersion = (): string => (
	git('--version')
		.replace('git version ', '')
);