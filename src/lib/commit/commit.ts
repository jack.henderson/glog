import { Issue } from '../issue';

/** Properties common to all commits. */
type CommitCommon = {
	hash: string;
	ref: string;
	date: Date;
	subject: string;
	body: string;
};

/**
 * A commit adhering to 
 * [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
 */
export type ConventionalCommit = CommitCommon & {
	isConventional: true;

	type: string;
	scope?: string;
	issue?: Issue;
};

/**
 * A commit not adhering to Conventional Commits.
 */
export type UnconventionalCommit = CommitCommon & {
	isConventional: false;
};

export type BaseCommit = ConventionalCommit | UnconventionalCommit;

export type RegularCommit = BaseCommit & {
	kind: 'regular'
};

/**
 * A commit merging two branches.
 */
export type MergeCommit = BaseCommit & {
	kind: 'merge';

	/** Branch being merged from. */
	source: string;
	/** Branch being merged into. */
	target: string;
};

/**
 * A commit which reverts some other commit.
 */
export type RevertCommit = BaseCommit & {
	kind: 'revert';

	reverts: string;
};

export type Commit = (
	| RegularCommit
	| MergeCommit
	| RevertCommit
);

/** An un-parsed commit. */
export type RawCommit = CommitCommon;