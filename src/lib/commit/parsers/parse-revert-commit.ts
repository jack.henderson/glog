import { BaseCommit, RevertCommit } from '../commit';


export const isRevertCommit = (commit: BaseCommit): boolean => {
	return commit.subject.startsWith('Revert "')
		&& commit.body.startsWith('This reverts commit ')
	;
};

export const parseRevertCommit = (commit: BaseCommit): RevertCommit => ({
	kind: 'revert',
	...commit,
	reverts: commit.body
		.replace('This reverts commit ', '')
		.replace('.', '')
		.trim(),
});