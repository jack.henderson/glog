import { RawCommit, ConventionalCommit } from '..';
import { Issue } from '../../issue';


export const isConventionalCommit = (commit: RawCommit): boolean => (
	!!commit.subject.match(/^[a-zA-Z]+(\(.+\))?:/)
);

export const parseConventionalCommit = (commit: RawCommit): ConventionalCommit => ({
	isConventional: true,
	...commit,
	...parseSubjectTypeAndScope(commit),
	...parseBodyAndIssue(commit),
});

const parseSubjectTypeAndScope = (commit: RawCommit): Pick<ConventionalCommit, 'subject' | 'type' | 'scope'> => {
	const subjectBegin = commit.subject.indexOf(':');
	const typeAndScope = commit.subject.substring(0, subjectBegin);
	const subject = commit.subject.substring(subjectBegin + 1);
	return {
		...parseTypeAndScope(typeAndScope),
		subject: subject.trim(),
	};
};

const parseTypeAndScope = (typeAndScope: string): Pick<ConventionalCommit, 'type' | 'scope'> => {
	const scopeBegin = typeAndScope.indexOf('(');
	if(scopeBegin === -1) {
		return {
			type: typeAndScope.trim(),
			scope: undefined,
		};
	}

	const scopeEnd = typeAndScope.indexOf(')');
	const type = typeAndScope.substring(0, scopeBegin);
	const scope = typeAndScope.substring(scopeBegin + 1, scopeEnd);

	return { type, scope };
};

const parseBodyAndIssue = (commit: RawCommit): Pick<ConventionalCommit, 'body' | 'issue'> => {
	let issue: Issue | undefined = undefined;
	const body = commit.body
		.split('\n')
		.filter(line => {
			if(isIssueLine(line)) {
				issue = parseIssueLine(line);
				return false;
			}

			return true;
		})
		.join('\n');
	
	return { body, issue };
};

const isIssueLine = (line: string): boolean => (
	!!line.match(/^([a-zA-Z]+)? ?#[0-9]+$/)
);

const parseIssueLine = (line: string): Issue => {
	const keywordAndRef = line.split('#');
	const keyword = keywordAndRef[0].trim();
	const reference = keywordAndRef[1];

	return {
		reference,
		closes: isClosingIssueKeyword(keyword),
	};
};

const isClosingIssueKeyword = (keyword: string): boolean => {
	switch(keyword.toLowerCase()) {
    case 'close':
    case 'closes':
    case 'closed':
    case 'fix':
    case 'fixes':
    case 'fixed':
    case 'resolve':
    case 'resolves':
    case 'resolved':
			return true;

		default:
			return false;
	}
};