import { BaseCommit, MergeCommit } from '../commit';


export const isMergeCommit = (commit: BaseCommit): boolean => (
	commit.subject.startsWith("Merge branch '")
		&& commit.subject.includes("' into '")
		&& commit.subject.match(/'/g)?.length === 4
);

export const parseMergeCommit = (commit: BaseCommit): MergeCommit => {
	const parts = commit.subject.split("'");
	return {
		kind: 'merge',
		...commit,
		source: parts[1],
		target: parts[3],
	};
};