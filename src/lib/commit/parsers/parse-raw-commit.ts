import { Commit, RawCommit } from '..';
import { BaseCommit } from '../commit';
import { isConventionalCommit, parseConventionalCommit } from './parse-conventional-commit';
import { isMergeCommit, parseMergeCommit } from './parse-merge-commit';
import { isRevertCommit, parseRevertCommit } from './parse-revert-commit';



export const parseRawCommit = (commit: RawCommit): Commit => (
	cleanCommit(parseCommit(parseBaseCommit(commit)))
);

const parseBaseCommit = (commit: RawCommit): BaseCommit => {
	if(isConventionalCommit(commit)) {
		return parseConventionalCommit(commit);
	}
	return {
		isConventional: false,
		...commit,
	};
};

const parseCommit = (commit: BaseCommit): Commit => {
	if(isMergeCommit(commit)) {
		return parseMergeCommit(commit);
	}

	if(isRevertCommit(commit)) {
		return parseRevertCommit(commit);
	}

	return {
		kind: 'regular',
		...commit,
	};
};

const cleanCommit = (commit: Commit): Commit => {
	return {
		...commit,
		subject: commit.subject.trim(),
		body: commit.body,
	};
};