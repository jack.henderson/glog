import { gitCurrentBranch } from '../lib';


export type GlogOptions = {
	sourceBranch: string;
	targetBranch: string;
	noClip: boolean;
	file?: string;
};

const getDefaults = (): GlogOptions => ({
	sourceBranch: gitCurrentBranch(),
	targetBranch: 'develop',
	noClip: false,
});

export const glogOptions = (options: Partial<GlogOptions> = {}): GlogOptions => ({
	...getDefaults(),
	...options,
});