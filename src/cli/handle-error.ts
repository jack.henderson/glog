import { GlogError } from '../lib/error';
import { putError } from './put';


export const handleError = (err: unknown): void => {
	if(GlogError.is(err)) {
		putError(err.message.trim());
		process.exit(err.status);
	} else if(err instanceof Error) {
		putError(errorMessage(err.message, err.stack));
	} else {
		putError(errorMessage(`${err} was thrown.`));
	}
};

const errorMessage = (title: string, stack?: string) => (
`Crashed! ${title}
This is probably a bug. You can report this issue at https://gitlab.com/jack.henderson/glog/-/issues/new.${ stack ? `\n\n${stack}` : ''}`
);