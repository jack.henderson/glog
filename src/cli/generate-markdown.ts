import { Commit } from '../lib';
import { Issue } from '../lib/issue';
import { GlogOptions } from './options';


export const generateMarkdown = (
	options: GlogOptions, commits: Commit[], issues: Issue[]
): string => {
	const commitMarkdown = commits
		.map(c => generateCommitMarkdown(options, c))
		.join('\n');

	if(!issues.length) {
		return commitMarkdown;
	}

	const issuesMarkdown = issues
		.sort((a, b) => {
			if(a.closes !== b.closes) {
				return a.closes ? -1 : 1;
			}

			return a.reference.localeCompare(b.reference);
		})
		.map(issue => generateIssuesMarkdown(options, issue))
		.join('\n');

	return `${commitMarkdown}\n\n${issuesMarkdown}`;
};

const generateCommitMarkdown = (options: GlogOptions, commit: Commit): string => {
	const subject = ` - ${generateSubjectLine(options, commit)}`;
	const body = generateBody(options, commit);

	return `${subject}${body.trim() ? `\n${body}` : ''}`;
};

const generateSubjectLine = (options: GlogOptions, commit: Commit): string => {
	if(!commit.isConventional) {
		return commit.subject;
	}

	const scope = commit.scope ? `(${commit.scope})` : '';
	return `**${commit.type}**${scope}: ${commit.subject} (${commit.ref})`;
};

const generateBody = (options: GlogOptions, commit: Commit): string => {
	const lines = commit.body
		.split('\n')
		.filter(line => line)
		.map(line => `   ${line}`);

	return lines.join('\n');
};

const generateIssuesMarkdown = (options: GlogOptions, issue: Issue): string => {
	const keyword = issue.closes ? 'closes ' : '';
	return `${keyword}#${issue.reference}`;
};