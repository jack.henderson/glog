import { Commit } from '../lib';
import { parseRawCommit, RawCommit } from '../lib/commit';
import { gitLog, gitLogUnique } from '../lib/git';
import { Issue } from '../lib/issue';
import { GlogOptions } from './options';


type CommitLogResult = {
	commits: Commit[];
	issues: Issue[];
};

export const getCommitLog = (options: GlogOptions): CommitLogResult => {
	const commits= logCommits(options)
		.reverse()
		.map(parseRawCommit);

	const issues = collateIssues(commits);

	return { commits, issues };
};

const logCommits = (options: GlogOptions): RawCommit[] => {
	if(options.targetBranch === '--all--') {
		return gitLog(options.sourceBranch);
	}
	return gitLogUnique(options.sourceBranch, options.targetBranch);
};

const collateIssues = (commits: Commit[]): Issue[] => {
	const issues: Record<string, boolean> = {};
	commits.forEach(c => {
		if(!c.isConventional || !c.issue) {
			return;
		}
		const reference = c.issue.reference;
		issues[reference] = issues[reference] || c.issue.closes;
	});

	return Object.entries(issues)
		.map(([ reference, closes ]) => ({ reference, closes }))
		.reduce<Issue[]>((accum, issue) => [ ...accum, issue ], []);
};