#!/usr/bin/env node

import { assertGitIsInstalled } from '../lib';
import { parseArgs } from './args';
import { generateMarkdown } from './generate-markdown';
import { getCommitLog } from './get-commit-log';
import { handleError } from './handle-error';
import { glogOptions } from './options';
import { putSuccess } from './put';
import { writeSync as copy } from 'clipboardy';
import { writeFileSync } from 'fs';


process.on('uncaughtException', handleError);


assertGitIsInstalled();

const options = glogOptions(parseArgs());

const { commits, issues } = getCommitLog(options);
if(!commits.length) {
	putSuccess(`No commits to show (${options.sourceBranch} -> ${options.targetBranch})`);
	process.exit(0);
}

const markdown = generateMarkdown(options, commits, issues);
console.error();
console.log(`${markdown}`);
console.error();


if(!options.noClip) {
	copy(markdown);
	putSuccess('Copied to clipboard');
}

if(options.file) {
	writeFileSync(options.file, `${markdown}\n`, { encoding: 'utf-8' });
	putSuccess(`Written to ${options.file}`);
}