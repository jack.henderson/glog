import yargs from 'yargs';
import { GlogError } from '../lib/error';

import { GlogOptions } from './options';

const DEFAULT_SOURCE = '<current branch>';
const DEFAULT_TARGET = 'develop';

export const parseArgs = (args=process.argv.slice(2)): Partial<GlogOptions> => {
	const parsed = yargs(args)
		.usage('glog [options]')
		.option('s', {
			alias: 'source',
			describe: 'Branch to log commits from.',
			type: 'string',
			default: DEFAULT_SOURCE,
		})
		.option('t', {
			alias: 'target',
			describe: 'Log commits which exist in `source`, but not in `target`',
			type: 'string',
			default: DEFAULT_TARGET,
		})
		.option('a', {
			alias: 'all',
			describe: 'Log all commits in `source`.\nIncompatible with --target',
			type: 'boolean',
			default: false,
		})
		.options('f', {
			alias: 'file',
			describe: 'Output the result into the specified file.',
			type: 'string',
		})
		.options('noclip', {
			describe: 'Do not copy the result to the clipboard.',
			type: 'boolean',
		})
		.help('help', 'Show this help message')
		.alias('h', 'help')
		.alias('v', 'version')
		.argv;

	const result: Partial<GlogOptions> = {};

	if(parsed.s === DEFAULT_SOURCE) {
		parsed.s = '';
	}

	if(parsed.t === DEFAULT_TARGET) {
		parsed.t = '';
	}

	if(parsed.a) {
		if(parsed.t) {
			throw new GlogError('Cannot specify --all as well as --target.');
		}

		result.targetBranch = '--all--';
	} else if(parsed.t) {
		result.targetBranch = parsed.t || undefined;
	}

	if(parsed.s) {
		result.sourceBranch = parsed.s || undefined;
	}

	if(parsed.f) {
		result.file = parsed.f;
	}

	return result;
};
