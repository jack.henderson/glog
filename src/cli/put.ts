import chalk from 'chalk';

// Type PutLevel = 'info' | 'success' | 'error';

enum PutLevel {
	info,
	success,
	error
}

const put = (level: PutLevel, message: string): void => {
	console.error(
		` ${putColor(level)(putSymbol(level))} ${formatMessage(level, message)}`
	);
};

export const putInfo    = (message: string):void  => put(PutLevel.info, message);
export const putSuccess = (message: string):void  => put(PutLevel.success, message);
export const putError   = (message: string):void  => put(PutLevel.error, message);

const putSymbol = (level: PutLevel): string => {
	switch(level) {
		case PutLevel.info:    return '-';
		case PutLevel.success: return '✔';
		case PutLevel.error:   return '⨯';
	}
};

const putColor = (level: PutLevel): ((x: string) => string) => {
	switch(level) {
		case PutLevel.info:    return chalk.dim;
		case PutLevel.success: return chalk.greenBright;
		case PutLevel.error:   return chalk.redBright;
	}
};


const formatMessage = (level: PutLevel, message: string): string => {
	const lines = message.split('\n');
	lines[0] = putColor(level)(chalk.bold(lines[0]));
	return lines.join('\n   ');
};