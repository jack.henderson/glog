<center>
**g l o g**
===========
</center>
<center>

![npm](https://img.shields.io/npm/v/@glog/cli)
[![pipeline status](https://gitlab.com/jack.henderson/glog/badges/release/pipeline.svg)](https://gitlab.com/jack.henderson/glog/-/commits/develop)
[![coverage report](https://gitlab.com/jack.henderson/glog/badges/release/coverage.svg)](https://gitlab.com/jack.henderson/glog/-/commits/develop) 

</center>

----

<center>

## _Create beautiful* merge requests & changelogs from your git commits_
<sup>* beauty in the eye of the beholder. your mileage may vary.</sup>

</center>

---
## **Installation**
```bash
npm i -g @glog/cli
```
```bash
yarn global add @glog/cli
```

---
## **Usage**
```
glog [options]

Options:
  -s, --source   Branch to log commits from.
                                          [string] [default: "<current branch>"]
  -t, --target   Log commits which exist in `source`, but not in `target`
                                                   [string] [default: "develop"]
  -a, --all      Log all commits in `source`.
                 Incompatible with --target           [boolean] [default: false]
  -h, --help     Show this help message                                [boolean]
  -v, --version  Show version number                                   [boolean]

```