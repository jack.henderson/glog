import { expect } from 'chai';
import { parseRawCommit } from '../../../../src/lib/commit';
import { fakeCommit } from './helpers';


describe('lib : parseRevertCommit', () => {
	it('parses revert commit', () => {
		const commit = parseRawCommit(fakeCommit({
			'subject': 'Revert "test commit"',
			'body': 'This reverts commit revertedHashHere.',
		}));

		expect(commit)
			.to.deep.include({
				isConventional: false,
				kind: 'revert',
				subject: 'Revert "test commit"',
				body: 'This reverts commit revertedHashHere.',
				reverts: 'revertedHashHere',
			});
	});
});