import { RawCommit } from '../../../../src/lib';


export const fakeCommit = (from: Partial<RawCommit>): RawCommit => ({
	hash: 'fakehash',
	ref: 'fakeref',
	date: new Date(),
	subject: 'a fake commit',
	body: 'a fake body',
	...from,
});