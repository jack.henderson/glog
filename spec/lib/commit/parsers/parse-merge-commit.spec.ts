import { expect } from 'chai';
import { parseRawCommit } from '../../../../src/lib/commit';
import { fakeCommit } from './helpers';


describe('lib : parseMergeCommit()', () => {
	it('parses merge commit', () => {
		const commit = fakeCommit({
			'subject': "Merge branch 'feat/commit-types' into 'develop'",
		});
		expect(parseRawCommit(commit))
			.to.deep.include({
				isConventional: false,
				kind: 'merge',
				source: 'feat/commit-types',
				target: 'develop',
			});
	});
});