import { expect } from 'chai';

import { parseRawCommit } from '../../../../src/lib/commit';
import { fakeCommit } from './helpers';


describe('lib : parseConventionalCommit()', () => {
	it('parses convectional commit type', () => {
		const commit = parseRawCommit(fakeCommit({
			subject: 'feat: blabla',
		}));
		expect(commit)
			.to.deep.include({
				isConventional: true,
				kind: 'regular',
				type: 'feat',
				scope: undefined,
				subject: 'blabla',
				body: 'a fake body',
				issue: undefined,
			});
	});

	it('parses conventional commit scope', () => {
		const commit = parseRawCommit(fakeCommit({
			subject: 'feat(the-scope): blabla',
		}));
		expect(commit)
			.to.deep.include({
				isConventional: true,
				kind: 'regular',
				type: 'feat',
				scope: 'the-scope',
				subject: 'blabla',
				body: 'a fake body',
				issue: undefined,
			});
	});

	it('parses conventional commit non-closing issue', () => {
		[
			'',
			'issue',
		].forEach(issue => {
			expect(parseRawCommit(fakeCommit({
				subject: 'feat(the-scope): blabla',
				body: `${issue} #555`,
			})))
				.to.deep.include({
					isConventional: true,
					kind: 'regular',
					type: 'feat',
					scope: 'the-scope',
					subject: 'blabla',
					body: '',
					issue: {
						reference: '555',
						closes: false,
					},
				});
		});
	});

	it('parses conventional commit closing issue', () => {
		[
			'close',
			'closes',
			'closed',
			'fix',
			'fixes',
			'fixed',
			'resolve',
			'resolves',
			'resolved',
		].forEach(issue => {
			expect(parseRawCommit(fakeCommit({
				subject: 'feat(the-scope): blabla',
				body: `${issue} #555`,
			})))
				.to.deep.include({
					isConventional: true,
					kind: 'regular',
					type: 'feat',
					scope: 'the-scope',
					subject: 'blabla',
					body: '',
					issue: {
						reference: '555',
						closes: true,
					},
				});
		});
	});

	it('preserves body when issue is present', () => {
		expect(parseRawCommit(fakeCommit({
			subject: 'feat: blabla',
			body: 'some commit body\n\ncloses #555',
		})))
			.to.deep.include({
				body: 'some commit body\n',
				issue: {
					reference: '555',
					closes: true,
				},
			});
	});

	it('parses subject with multiple colons', () => {
		const commit = fakeCommit({
			subject: 'build: add `build:client:static` job',
		});

		expect(parseRawCommit(commit))
			.to.deep.include({
				subject: 'add `build:client:static` job',
				type: 'build',
			});
	});

	it('parses issue keywords with upper-case characters', () => {
		const commit = fakeCommit({
			subject: 'test: issue',
			body: 'Closes #555',
		});
		expect(parseRawCommit(commit))
			.to.deep.include({
				issue: {
					reference: '555',
					closes: true,
				},
			});
	});
});