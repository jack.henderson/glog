import { describe, it } from 'mocha';
import { expect } from 'chai';

import { GlogError } from '../../src/lib/error';


describe('lib : GlogError', () => {
	describe('GlogError.is()', () => {
		it('returns false for Error', () => {
			expect(GlogError.is(new Error()))
				.to.be.false;
		});

		it('returns true for GlogError', () => {
			expect(GlogError.is(new GlogError('test')))
				.to.be.true;
		});
	});
});