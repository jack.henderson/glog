import { expect } from 'chai';

import { parseArgs } from '../../src/cli/args';


describe('cli : parseArgs()', () => {
	it('returns empty Options for empty args', () => {
		expect(parseArgs([]))
			.to.deep.eq({});
	});

	it('throws for --target & --all', () => {
		expect(() => parseArgs(['--all', '--target', 'test/branch']))
			.to.throw('Cannot specify --all as well as --target.');
	});

	const expectations: [ args: string, expectedResult: Record<string, string>][] = [
		[ '--source test/source', { sourceBranch: 'test/source' } ],
		[ '-s test/source',       { sourceBranch: 'test/source' } ],

		[ '--source test/source --all', { sourceBranch: 'test/source', targetBranch: '--all--' } ],
		[ '-s test/source -a',          { sourceBranch: 'test/source', targetBranch: '--all--' } ],

		[ '--target test/target', { targetBranch: 'test/target' } ],
		[ '-t test/target',       { targetBranch: 'test/target' } ],
		
		[ '--source test/source --target test/target', { sourceBranch: 'test/source', targetBranch: 'test/target' } ],
		[ '-s test/source -t test/target',       { sourceBranch: 'test/source', targetBranch: 'test/target' } ],
	];
	expectations.forEach(([ args, expectedResult ]) => {
		it(`parses ${args}`, () => {
			expect(parseArgs(args.split(' ')))
				.to.deep.eq(expectedResult);
		});
	});
});